import json
import os
import datetime

'''
with open('bang.v240.js') as json_data:
    f = json.load(json_data)
print(len(f))
for i in xrange(len(f)):
    print("##################################################################")
    print(f[i]["d"])
    print(f[i]["p"])
    print(f[i]["s"]).encode('utf-8')
    print(f[i]["r"])
    print(f[i]["u"])
    print(f[i]["t"])
'''
versions = [232,233,234,235,236,237,238,239,240,242,243]
for z in xrange(len(versions)-1):
    print("compare v"+str(versions[z])+" to v"+str(versions[z+1])+":")
    statOld = os.stat('bang.v'+str(versions[z])+'.js')
    statNew = os.stat('bang.v'+str(versions[z+1])+'.js')
    print("    v"+str(versions[z])+": "+str(datetime.datetime.fromtimestamp(statOld.st_mtime)))
    print("    v"+str(versions[z+1])+": "+str(datetime.datetime.fromtimestamp(statNew.st_mtime)))
    # copy json data from files
    with open('bang.v'+str(versions[z])+'.js') as json_data:
        f1 = json.load(json_data)
    with open('bang.v'+str(versions[z+1])+'.js') as json_data:
        f2 = json.load(json_data)
    # put urls into regular python lists
    old = []
    for i in f1:
        old.append(i["d"])
    new = []
    for i in f2:
        new.append(i["d"])
    print("    number in v"+str(versions[z])+": "+str(len(old)))
    print("    number in v"+str(versions[z+1])+": "+str(len(new)))
    # compare them
    removed = []
    added = []
    for url in old:
        if url not in new:
            removed.append(url)
    for url in new:
        if url not in old:
            added.append(url)

    print("    removed: "+str(len(removed)))
    # print all removed
    for item in removed:
        print("        "+item)
    '''
    # print a sample of sites
    if len(removed) != 0:
        print("    sample:")
        if len(removed) < 10:
            numToPrintRemoved = len(removed)
        else:
            numToPrintRemoved = 10
        for i in range(0,numToPrintRemoved):
            print("        "+removed[i])
    '''
    print("    added: "+str(len(added)))
    # print all added
    for item in added:
        print("        "+item)
    '''
    # print a sample of sites
    if len(added) != 0:
        if len(added) < 10:
            numToPrintAdded = len(added)
        else:
            numToPrintAdded = 10
        print("    sample:")
        for i in range(0,numToPrintAdded):
            print("        "+added[i])
    '''
